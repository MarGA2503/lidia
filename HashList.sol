pragma solidity ^0.5.0;

contract HashesList{
bytes32 [] hashesList;
event newHashAdded(bytes32 newHash);

function addHashtoArray (bytes32 newHash) public  {
  hashesList.push(newHash);
  emit newHashAdded(newHash);
}

function getHashList () public view returns (bytes32[] memory) {
  return hashesList;
}

}

pragma solidity ^0.5.0;

contract Register{
mapping(address => string) public countriesList;

function addCountryAddress (string memory countryName ) public returns (bool) {
  if(keccak256(bytes(countriesList[msg.sender])) != keccak256(bytes(""))) return false; // duplicate key
  countriesList[msg.sender] = countryName;
  return true;
}

function getCountryName (address countryAddress) public view returns (string memory) {
  return countriesList[countryAddress];
}

}

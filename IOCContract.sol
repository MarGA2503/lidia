pragma solidity ^0.5.0;

contract IOCContract{
struct IOCdata {
  int timestamp;
  address sender;
  string title;
  string identifier;
  string description;
  string typ;
  string url;
}

mapping (bytes32 => IOCdata) public IOClist;

function addIOC(bytes32 newIOC, int IOCtimestamp, string memory IOCtitle, string memory IOCid, string memory IOCdes, string memory IOCtype, string memory IOCurl) public returns (bool){
  if(IOClist[newIOC].sender != address(0)) return false;
  IOClist[newIOC] = IOCdata (IOCtimestamp, msg.sender, IOCtitle, IOCid, IOCdes, IOCtype, IOCurl);
  return true;
}

function getIOC (bytes32 IOC) public view returns (int IOCtimestamp, address IOCsender, string memory IOCtitle, string memory IOCid, string memory IOCdes, string memory IOCtype, string memory IOCurl){
  return (IOClist[IOC].timestamp, IOClist[IOC].sender, IOClist[IOC].title, IOClist[IOC].identifier, IOClist[IOC].description, IOClist[IOC].typ, IOClist[IOC].url);
}

}

from web3 import Web3, HTTPProvider, IPCProvider
import web3
import time

addHash_abi= [{"constant":False,"inputs":[{"name":"newHash","type":"bytes32"}],"name":"addHashtoArray","outputs":[],"payable":False,"stateMutability":"nonpayable","type":"function"},{"constant":True,"inputs":[],"name":"getHashList","outputs":[{"name":"","type":"bytes32[]"}],"payable":False,"stateMutability":"view","type":"function"},{"anonymous":False,"inputs":[{"indexed":False,"name":"newHash","type":"bytes32"}],"name":"newHashAdded","type":"event"}]
addHash_address = '0x4743c78977e35d7ff6e98e5695a9878c42e73b36'

def connectIPC(ipc_path):
    global w3
    w3 = Web3(Web3.IPCProvider(ipc_path))

def listen ():
    while True:
        # TODO: check consumption and  consider add sleep
        # new_entries = event_filter.get_new_entries()
        # if new_entries:
        #     print("new_entries", new_entries)
        #     new_hash = new_entries['data']
        filter_changes = w3.eth.getFilterChanges(event_filter.filter_id)
        if filter_changes:
            print("filter_changes", filter_changes)
            for element in filter_changes:
                new_hash = element['data']
                #TODO: Check length and send mail for length and data in array element
                print("new_hash", new_hash)
        #time.sleep(3)

def inizializate():
    global event_filter
    #addHash_contract = w3.eth.contract(address= w3.toChecksumAddress(addHash_address), abi= addHash_abi)
    event_filter = w3.eth.filter({"address": w3.toChecksumAddress(addHash_address)})

if __name__ == '__main__':
    #Some test variable values
    #TODO remove later when structure clear
    global ipc_path
    ipc_path = "/Users/margimenezaguilar/Documents/nodecon/geth.ipc"
    #Connect to geth
    connectIPC(ipc_path)
    inizializate()
    listen ()

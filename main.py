from web3 import Web3, HTTPProvider, IPCProvider
from ecdsa import SigningKey, SECP256k1
import time
import json
import web3
import os


#json.load
register_abi= [{"constant":True,"inputs":[{"name":"countryAddress","type":"address"}],"name":"getCountryName","outputs":[{"name":"","type":"string"}],"payable":False,"stateMutability":"view","type":"function"},{"constant":True,"inputs":[{"name":"","type":"address"}],"name":"countriesList","outputs":[{"name":"","type":"string"}],"payable":False,"stateMutability":"view","type":"function"},{"constant":False,"inputs":[{"name":"countryName","type":"string"}],"name":"addCountryAddress","outputs":[{"name":"","type":"bool"}],"payable":False,"stateMutability":"nonpayable","type":"function"}]
register_address = "0x4f418ef9dfc7d91a0bb10f9d31325b3ed5083fe7"

addHash_abi= [{"constant":False,"inputs":[{"name":"newHash","type":"bytes32"}],"name":"addHashtoArray","outputs":[],"payable":False,"stateMutability":"nonpayable","type":"function"},{"constant":True,"inputs":[],"name":"getHashList","outputs":[{"name":"","type":"bytes32[]"}],"payable":False,"stateMutability":"view","type":"function"},{"anonymous":False,"inputs":[{"indexed":False,"name":"newHash","type":"bytes32"}],"name":"newHashAdded","type":"event"}]
addHash_address = '0x4743c78977e35d7ff6e98e5695a9878c42e73b36'

IOCContract_abi = [{"constant":True,"inputs":[{"name":"IOC","type":"bytes32"}],"name":"getIOC","outputs":[{"name":"IOCtimestamp","type":"int256"},{"name":"IOCsender","type":"address"},{"name":"IOCtitle","type":"string"},{"name":"IOCid","type":"string"},{"name":"IOCdes","type":"string"},{"name":"IOCtype","type":"string"},{"name":"IOCurl","type":"string"}],"payable":False,"stateMutability":"view","type":"function"},{"constant":True,"inputs":[{"name":"","type":"bytes32"}],"name":"IOClist","outputs":[{"name":"timestamp","type":"int256"},{"name":"sender","type":"address"},{"name":"title","type":"string"},{"name":"identifier","type":"string"},{"name":"description","type":"string"},{"name":"typ","type":"string"},{"name":"url","type":"string"}],"payable":False,"stateMutability":"view","type":"function"},{"constant":False,"inputs":[{"name":"newIOC","type":"bytes32"},{"name":"IOCtimestamp","type":"int256"},{"name":"IOCtitle","type":"string"},{"name":"IOCid","type":"string"},{"name":"IOCdes","type":"string"},{"name":"IOCtype","type":"string"},{"name":"IOCurl","type":"string"}],"name":"addIOC","outputs":[{"name":"","type":"bool"}],"payable":False,"stateMutability":"nonpayable","type":"function"}]
IOCcontract_address = '0x799Ed91bA0972857ACFdF1eFa7cd4011E85D5e9f'

def connectIPC(ipc_path):
    global w3
    w3 = Web3(Web3.IPCProvider(ipc_path))

def generateAddress():
    valid_address = False
    #Checking if in use
    while not valid_address:
        #Generating the address
        priv = SigningKey.generate(curve=SECP256k1)
        pub = priv.get_verifying_key().to_string()
        address = w3.sha3(pub).hex()[-40:]
        valid_address = register_contract.functions.addCountryAddress('').call({'from':w3.toChecksumAddress(address)})
    return priv.to_string().hex(), address

def registerCountry (country, address, private_key):
    transaction = register_contract.functions.addCountryAddress(country).buildTransaction({'from':w3.toChecksumAddress(address),'gasPrice':  w3.eth.gasPrice})
    print(transaction)
    transaction['nonce'] = w3.eth.getTransactionCount(w3.toChecksumAddress(address),block_identifier="pending")
    checkBalanceEnough (address,transaction['gas'])
    signed = w3.eth.account.signTransaction(transaction, '0x' + private_key)
    tx_hash = w3.eth.sendRawTransaction(signed.rawTransaction)
    return tx_hash

def getCountryByAddress (address):
    return register_contract.functions.getCountryName(w3.toChecksumAddress(address)).call()

def waitForReceipt(tx_hash, timeout=150):
    tx_receipt = w3.eth.waitForTransactionReceipt(tx_hash, timeout)
    return tx_receipt

def sendToMine(address, thread=1):
     w3.miner.setEtherBase(w3.toChecksumAddress(address))
     w3.miner.start(thread)

def stopMining():
    w3.miner.stop()
    #Return etherbase to default account
    w3.miner.setEtherBase(w3.toChecksumAddress(default_account))

def checkBalanceEnough (address,estimateGas):
    fee = estimateGas * w3.eth.gasPrice
    current_balance = w3.eth.getBalance(w3.toChecksumAddress(address))
    while fee > current_balance:
        sendToMine(address)
        time.sleep(3)
        current_balance = w3.eth.getBalance(w3.toChecksumAddress(address))
    stopMining()

def checkIOC(hash, timestamp, title, description, type, url, address):
    print("checkIOC")
    return IOCContract_contract.functions.addIOC(hash, timestamp, title,identifier, description, type, url).call({'from':w3.toChecksumAddress(address)})

def insertIOC (timestamp,title, identifier, description, type, url, private_key,anonymus = False):
    tx_hash_IOCContract = ""
    tx_hash_addHash = ""
    receipt_IOC = ""
    receipt_addHash = ""
    if anonymus:
        private_key, address = generateAddress()
        print("address", address)
        print("private_key", private_key)
        print("First getter", getCountryByAddress (address))
    else:
        address = default_account
        print("address", default_account)
    #TODO check len of strings and stablish of what exactly would be hashed
    hash = w3.sha3(text='tohash5').hex()
    if checkIOC(hash, timestamp, title, description, type, url, address):
            gas = IOCContract_contract.functions.addIOC(hash, timestamp, title,identifier, description, type, url).estimateGas({'from':w3.toChecksumAddress(address),'gasPrice':  w3.eth.gasPrice})
            print("gas", gas)
            transaction = IOCContract_contract.functions.addIOC(hash, timestamp, title,identifier, description, type, url).buildTransaction({'from':w3.toChecksumAddress(address),'gas': gas,'gasPrice':  w3.eth.gasPrice})
            transaction['nonce'] = w3.eth.getTransactionCount(w3.toChecksumAddress(address),block_identifier="pending")
            signed = w3.eth.account.signTransaction(transaction, '0x' + private_key)
            print("transaction", transaction)
            tx_hash_IOCContract = w3.eth.sendRawTransaction(signed.rawTransaction)
            print("tx_hash_IOCContract", tx_hash_IOCContract.hex())
            receipt_IOC = waitForReceipt(tx_hash_IOCContract, timeout=150)
            print("receipt_IOC",receipt_IOC)
            if receipt_IOC:
                print (receipt_IOC)
                transaction =  addHash_contract.functions.addHashtoArray(hash).buildTransaction({'from':w3.toChecksumAddress(address),'gasPrice':  w3.eth.gasPrice})
                transaction['nonce'] = w3.eth.getTransactionCount(w3.toChecksumAddress(address),block_identifier="pending")
                signed = w3.eth.account.signTransaction(transaction, '0x' + private_key)
                tx_hash_addHash = w3.eth.sendRawTransaction(signed.rawTransaction)
                print("tx_hash_addHash", tx_hash_addHash)
                receipt_addHash = waitForReceipt(tx_hash_addHash, timeout=150)
    return tx_hash_IOCContract, tx_hash_addHash, receipt_IOC, receipt_addHash

if __name__ == '__main__':
    #Some test variable values
    #TODO remove later when structure clear
    global ipc_path
    first = True
    anonymous = False
    country = "España"
    ipc_path = "/Users/margimenezaguilar/Documents/nodecon/geth.ipc"
    timestamp = 1
    title = "Hi"
    identifier = "192.1.1.1"
    description = "Des"
    type = "IP"
    url = "www....."
    #Connect to geth
    connectIPC(ipc_path)
    #Chargue variables
    global register_contract
    global default_account
    register_contract = w3.eth.contract(address= w3.toChecksumAddress(register_address), abi= register_abi)
    addHash_contract = w3.eth.contract(address= w3.toChecksumAddress(addHash_address), abi= addHash_abi)
    IOCContract_contract = w3.eth.contract(address= w3.toChecksumAddress(IOCcontract_address), abi = IOCContract_abi)
    #Generate address if first time or Anonymous mode
    #TODO define file names for stored data
    if first or anonymous:
        #TODO: Save private key if first time and stablish as default
        private_key, address = generateAddress()
        print("address", address)
        print("private_key", private_key)
        print("First getter", getCountryByAddress (address))
        if first:
            default_account = address
            #We have to register
            w3.eth.defaultAccount = w3.toChecksumAddress(address)
            tx_hash = registerCountry(country, address, private_key)
            print("transaction_hash", tx_hash.hex())
            tx_receipt = waitForReceipt(tx_hash, timeout=150)
            print("transaction_receipt", tx_receipt)
    tx_hash_IOCContract, tx_hash_addHash, receipt_IOC, receipt_addHash = insertIOC (timestamp,title, identifier, description, type, url, private_key,anonymous)
    print("Second getter", getCountryByAddress (address))
    print("Default_account", default_account,  w3.eth.defaultAccount)
    print ("Receipts and transactions IOC", tx_hash_IOCContract, tx_hash_addHash, receipt_IOC, receipt_addHash)
